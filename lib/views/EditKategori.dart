import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/model/KategoriModel.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_ecommerce/model/api.dart';

class EditKategori extends StatefulWidget {
  final VoidCallback reload;
  final KategoriModel model;
  EditKategori(this.model, this.reload);

  @override
  _EditKategoriState createState() => _EditKategoriState();
}

class _EditKategoriState extends State<EditKategori> {
  String idKategori, namaKategori;

  final _key = new GlobalKey<FormState>();

  TextEditingController txtnamakategori;
  setup() async {
    txtnamakategori = TextEditingController(text: widget.model.namaKategori);
  }

  //tambahan ddl
  // KategoriModel _currentKategori;
  // final listKategori = new List<KategoriModel>();
  // final String linkKategori = BaseUrl.urlListKategori;

  // Future<List<KategoriModel>> _fetchKategori() async {
  //   var response = await http.get(Uri.parse(linkKategori));

  //   if (response.statusCode == 200) {
  //     final items = json.decode(response.body).cast<Map<String, dynamic>>();
  //     List<KategoriModel> listOfKategori = items.map<KategoriModel>((json) {
  //       return KategoriModel.fromJson(json);
  //     }).toList();

  //     return listOfKategori;
  //   } else {
  //     throw Exception('Failed to load internet');
  //   }
  // }

  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      prosesbiasa();
    }
  }

  prosesbiasa() async {
    final response = await http.post(BaseUrl.urlEditKategori, body: {
      "nama_kategori": namaKategori,
      "id_kategori": widget.model.idKategori
    });
    final data = jsonDecode(response.body);
    int value = data['success'];
    String pesan = data['message'];

    if (value == 1) {
      setState(() {
        print(pesan);
        widget.reload();
        Navigator.pop(context);
      });
    } else {
      print(pesan);
    }
  }

  String body(http.Response response) => response.body;

  // proseskategori() async {
  //   try {
  //     var uri = Uri.parse(BaseUrl.urlEditKategori);
  //     var request = http.MultipartRequest("POST", uri);
  //     request.fields['namaKategori'] = namaKategori;
  //     request.fields['idKtg'] = widget.model.idKategori;
  //   } catch (e) {
  //     prosesbiasa();
  //     debugPrint(e);
  //   }
  // }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setup();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Form(
        key: _key,
        child: ListView(
          padding: EdgeInsets.all(16.0),
          children: <Widget>[
            TextFormField(
              controller: txtnamakategori,
              validator: (e) {
                if (e.isEmpty) {
                  return "Silahkan isi data";
                }
              },
              onSaved: (e) => namaKategori = e,
              decoration: InputDecoration(labelText: "Nama Kategori"),
              style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.normal),
            ),
            MaterialButton(
              onPressed: () {
                check();
              },
              child: Text("Simpan"),
            )
          ],
        ),
      ),
    );
  }
}
