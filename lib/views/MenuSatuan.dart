import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/model/SatuanModel.dart';
import 'package:flutter_ecommerce/model/api.dart';
import 'package:flutter_ecommerce/views/EditSatuan.dart';
import 'package:flutter_ecommerce/views/TambahSatuan.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class MenuSatuan extends StatefulWidget {
  @override
  _MenuSatuanState createState() => _MenuSatuanState();
}

class _MenuSatuanState extends State<MenuSatuan> {
  var loading = false;
  final list = new List<SatuanModel>();
  final GlobalKey<RefreshIndicatorState> _refresh =
      GlobalKey<RefreshIndicatorState>();

  getPref() async {
    _lihatData();
  }

  Future<void> _lihatData() async {
    list.clear();
    setState(() {
      loading = true;
    });

    final response = await http.get(BaseUrl.urlListSatuan);
    if (response.contentLength == 2) {
    } else {
      final data = jsonDecode(response.body);
      data.forEach((api) {
        final ab = new SatuanModel(api['baris'], api['id_satuan'], api['nama_satuan'], api['satuan'],);
        list.add(ab);
      });

      setState(() {
        loading = false;
      });
    }
  }

  _prosesHapus(String idStn) async {
    final response =
        await http.post(BaseUrl.urlHapusSatuan, body: {"idSatuan": idStn});
    final data = jsonDecode(response.body);
    int value = data['success'];
    String pesan = data['message'];
    if (value == 1) {
      setState(() {
        Navigator.pop(context);
        _lihatData();
      });
    } else {
      print(pesan);
    }
  }

  dialogHapus(String idStn) {
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            child: ListView(
              padding: EdgeInsets.all(16.0),
              shrinkWrap: true,
              children: <Widget>[
                Text(
                  "Apakah anda yakin ingin menghapus Satuan ini ?",
                  style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 18.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Tidak",
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(width: 25.0),
                    InkWell(
                      onTap: () {
                        _prosesHapus(idStn);
                      },
                      child: Text(
                        "Ya",
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Text(
                "Data Satuan",
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => new TambahSatuan(_lihatData)));
        },
        child: Icon(Icons.add),
        backgroundColor: Color.fromRGBO(80, 165, 215, 1.0),
      ),
      body: RefreshIndicator(
          onRefresh: _lihatData,
          key: _refresh,
          child: loading
              ? Center(child: CircularProgressIndicator())
              : ListView.builder(
                  itemCount: list.length,
                  itemBuilder: (context, i) {
                    final x = list[i];
                    return Container(
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("No. " + x.baris,
                                    style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.bold)),
                                Text("Nama Satuan : " + x.namaSatuan,
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.normal)),
                                Text("Jumlah Satuan : " + x.satuan,
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.normal)),
                              ],
                            ),
                          ),
                          IconButton(
                              color: Colors.orange[600],
                              icon: Icon(Icons.edit),
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        EditSatuan(x, _lihatData)));
                              }),
                          IconButton(
                              color: Colors.redAccent[400],
                              icon: Icon(Icons.delete),
                              onPressed: () {
                                dialogHapus(x.idSatuan);
                              }),
                        ],
                      ),
                    );
                  },
                )),
    );
  }
}
