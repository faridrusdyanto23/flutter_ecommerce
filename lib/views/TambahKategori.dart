import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/model/api.dart';
import 'package:http/http.dart' as http;

class TambahKategori extends StatefulWidget {
  final VoidCallback reload;
  TambahKategori(this.reload);

  @override
  _TambahKategoriState createState() => _TambahKategoriState();
}

class _TambahKategoriState extends State<TambahKategori> {
  String namaKategori;
  final _key = new GlobalKey<FormState>();

  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      simpanbarang();
    }
  }

  simpanbarang() async {
    try {
      var uri = Uri.parse(BaseUrl.urlTambahKategori);
      var request = http.MultipartRequest("POST", uri);
      request.fields['nama_kategori'] = namaKategori;
      var response = await request.send();
      if (this.mounted) {
        setState(() {
          widget.reload();
          Navigator.pop(context);
        });
      }
    } catch (e) {
      debugPrint(e);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //getPref();
  }

  @override
  Widget build(BuildContext context) {
    var placeholder = Container(
      width: double.infinity,
      height: 150.0,
    );
    return Scaffold(
        backgroundColor: Color.fromRGBO(244, 244, 244, 1),
        appBar: AppBar(),
        body: Form(
          key: _key,
          child: ListView(
            padding: EdgeInsets.all(16.0),
            children: <Widget>[
              TextFormField(
                validator: (e) {
                  if (e.isEmpty) {
                    return "Silahkan isi nama produk";
                  }
                },
                onSaved: (e) => namaKategori = e,
                decoration: InputDecoration(labelText: "Nama Kategori"),
              ),
              MaterialButton(
                onPressed: () {
                  check();
                },
                child: Text("Simpan"),
              )
            ],
          ),
        ));
  }
}
