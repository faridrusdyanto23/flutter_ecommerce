import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/model/KeranjangModelLaris.dart';
import 'package:flutter_ecommerce/model/ProdukTerlarisModel.dart';
import 'package:flutter_ecommerce/model/api.dart';
import 'package:flutter_ecommerce/custom/constans.dart';
import 'package:intl/intl.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class DetailProdukLaris extends StatefulWidget {
  final ProdukTerlarisModel listlaris;

  DetailProdukLaris({@required this.listlaris});

  @override
  _DetailProdukLarisState createState() => _DetailProdukLarisState();
}

class _DetailProdukLarisState extends State<DetailProdukLaris> {
  int _currentImage = 0;
  final money = NumberFormat("#,##0", "en_US");
  String idUsers;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idUsers = preferences.getString("userid");
    });
    _countCart();
  }

  // String jumlahnya;

  List<Widget> listL = [];
  List<Widget> buildPageIndicator() {
    for (var i = 0; i < widget.listlaris.image.length; i++) {
      listL.add(
          i == _currentImage ? buildIndicator(true) : buildIndicator(false));
    }
    return listL;
  }

  Widget buildIndicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 6.0),
      height: 8.0,
      width: isActive ? 20.0 : 8.0,
      decoration: BoxDecoration(
        color: isActive ? Colors.white : Colors.grey[400],
        borderRadius: BorderRadius.all(
          Radius.circular(12),
        ),
      ),
    );
  }

  // add to cart
  tambahKeranjang(String idProduk, String harga) async {
    final responseLaris = await http.post(Uri.parse(BaseUrl.urlAddCart),
        body: {"userid": idUsers, "id_barang": idProduk, "harga": harga});

    final dataLaris = jsonDecode(responseLaris.body);
    int valueLaris = dataLaris['success'];
    String pesanLaris = dataLaris['message'];

    if (valueLaris == 1) {
      print(pesanLaris);
      _countCart();
    } else {
      print(pesanLaris);
    }
  }

  var loadingLaris = false;
  String jumlahnyaLaris = "0";
  final exLaris = List<KeranjangModelLaris>();
  _countCart() async {
    if (this.mounted) {
      setState(() {
        loadingLaris = true;
      });
    }
    exLaris.clear();
    final responseLaris = await http.get(Uri.parse(BaseUrl.urlCountCart + idUsers));
    final dataLaris = jsonDecode(responseLaris.body);
    dataLaris.forEach((api) {
      final expLaris = new KeranjangModelLaris(api['jumlah']);
      exLaris.add(expLaris);
      if (this.mounted) {
        setState(() {
          jumlahnyaLaris = expLaris.jumlahLaris;
        });
      }
    });
    if (this.mounted) {
      setState(() {
        _countCart();
        loadingLaris = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _countCart();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.keyboard_arrow_left,
            size: 32,
            color: Colors.black,
          ),
        ),
        actions: <Widget>[
          Stack(
            children: <Widget>[
              IconButton(
                  icon: Icon(
                    Icons.shopping_cart,
                    color: Colors.orangeAccent,
                  ),
                  onPressed: () {}),
              // tambahkan keterangan jumlah produk dalam cart
              jumlahnyaLaris == "0"
                  ? Container()
                  : Positioned(
                      right: 0.0,
                      child: Stack(
                        children: <Widget>[
                          Icon(Icons.brightness_1,
                              size: 20.0, color: Colors.white),
                          Positioned(
                            top: 3.0,
                            right: 6.0,
                            child: Text(
                              jumlahnyaLaris,
                              style: TextStyle(
                                  color: Colors.black, fontSize: 11.0),
                            ),
                          )
                        ],
                      ),
                    )
            ],
          ),
        ],
      ),
      body: Container(
        decoration: BoxDecoration(gradient: kGradient),
        child: SafeArea(
            child: Column(
          children: <Widget>[
            Expanded(
              child: PageView(
                physics: BouncingScrollPhysics(),
                onPageChanged: (int page) {
                  if (this.mounted) {
                    setState(() {
                      _currentImage = page;
                    });
                  }
                },
                children: <Widget>[
                  Container(
                    child: Hero(
                      tag: widget.listlaris.nama_barang,
                      child: Image.network(
                        BaseUrl.paths + widget.listlaris.image,
                        width: 100,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: size.height * 0.4,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    height: size.height * 0.3,
                    padding: EdgeInsets.all(32),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          widget.listlaris.nama_barang,
                          style: TextStyle(
                              fontSize: 26,
                              color: Colors.deepOrange,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              "Rp." +
                                  money.format(
                                      int.parse(widget.listlaris.harga)),
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.deepOrange,
                                  fontWeight: FontWeight.bold),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text("Price Ratings",
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.grey[400])),
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.star,
                                      size: 16,
                                      color: kStarsColor,
                                    ),
                                    Icon(
                                      Icons.star,
                                      size: 16,
                                      color: kStarsColor,
                                    ),
                                    Icon(
                                      Icons.star,
                                      size: 16,
                                      color: kStarsColor,
                                    ),
                                    Icon(
                                      Icons.star,
                                      size: 16,
                                      color: kStarsColor,
                                    ),
                                    Icon(
                                      Icons.star_half,
                                      size: 16,
                                      color: kStarsColor,
                                    ),
                                  ],
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: size.height * 0.1,
                    decoration: BoxDecoration(
                      color: Colors.orange,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      ),
                    ),
                    child: GestureDetector(
                      onTap: () {
                        tambahKeranjang(widget.listlaris.id_barang,
                            widget.listlaris.harga);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Add to Cart",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: Colors.white),
                          ),
                          SizedBox(width: 16),
                          Icon(
                            Icons.shopping_basket,
                            color: Colors.white,
                            size: 30,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        )),
      ),
    );
  }
}
