import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ecommerce/custom/currency.dart';
import 'package:flutter_ecommerce/model/api.dart';
import 'package:flutter_ecommerce/model/CartModel.dart';
import 'package:flutter_ecommerce/views/Checkout.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Cart extends StatefulWidget {
  // const Cart({ Key? key }) : super(key: key);

  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  final _key = new GlobalKey<FormState>();
  final _keyAlert = new GlobalKey<FormState>();
  final money = NumberFormat("#,##0", "en_US");
  final list = new List<CartModel>();
  final GlobalKey<RefreshIndicatorState> _refresh = 
    GlobalKey<RefreshIndicatorState>();
  var loading = false;
  String idUsers;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idUsers = preferences.getString("userid");
    });
    _countData();
    _lihatData();
  }

  String totalBelanja = "0";
  String nilaiBayar = "0";

  Future<void> _lihatData() async {
    list.clear();
    setState(() {
      loading = true;
    });
    final response = await http.get(Uri.parse(BaseUrl.urlDetailCart + idUsers));
    // print(BaseUrl.urlDetailCart + idUsers);
    if(response.contentLength == 2) {
      
    } else {
      final data = jsonDecode(response.body);
      // print(data); 
      // print(idUsers);
      data.forEach((api) {
        final ab = new CartModel(api['id_barang'], idUsers, api['nama_barang'], 
            api['gambar'], api['harga'], api['qty']);
        list.add(ab);
      });

      setState(() {
        _countData();
        loading = false;
      });
    }
  }


  Future<void> _countData() async{
    setState(() {
      loading = true;
    });
    final responseCnt = await http.get(Uri.parse(BaseUrl.urlCountCart + idUsers));
    // print(BaseUrl.urlCountCart);
    if(responseCnt.contentLength == 2) {

    } else {
      final dataCnt = jsonDecode(responseCnt.body);
      dataCnt.forEach((api) {
        totalBelanja = api['totalharga'];
      });

      setState(() {
        loading = false;
      });
    }
  }

  dialogCheckout(String iduser, String grandTotal) {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          child: Form(
            key: _key,
            child: ListView(
              padding: EdgeInsets.all(16.0),
              shrinkWrap: true,
              children: <Widget>[
                Text("Form Pembayaran"),
                TextFormField(
                  inputFormatters: [
                    WhitelistingTextInputFormatter.digitsOnly,
                    CurrencyFormat()
                  ],
                  validator: (e) {
                    if(e.isEmpty) {
                      return "Silahkan isi Nilai bayar";
                    }
                  },
                  onSaved: (e) => nilaiBayar = e,
                  decoration: InputDecoration(labelText: "Nilai Bayar"),
                ),
                SizedBox(height: 18.0,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Batal",
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                      )),
                    SizedBox(
                      width: 25.0,
                    ),
                    InkWell(
                      onTap: () {
                        check(iduser, grandTotal);
                        // print(BaseUrl.urlCheckout);
                      },
                      child: Text(
                        "Proses",
                        style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold),
                      )),
                  ],
                )
              ],
            ),
          ),
        );
      }
    );
  }

  dialogCart(String txt) {
    showDialog(
      context: context, 
      builder: (context) {
        return Dialog(
          child: Form(
            key: _keyAlert,
            child: ListView(
              padding: EdgeInsets.all(16.0),
              shrinkWrap: true,
              children: <Widget>[
                Text(
                  txt,
                  style: 
                    TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 18.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "OK",
                        style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold),
                      )),
                  ],
                )
              ],
            ),
        ),
      );
    });
  }

  dialogDelProductInCart(String idProduk, String harga, String paramUserID) {
    showDialog(
      context: context, 
      builder: (context) {
        return Dialog(
          child: Form(
            key: _key,
            child: ListView(
              padding: EdgeInsets.all(16.0),
              shrinkWrap: true,
              children: <Widget>[
                Text(
                  "Ingin Menghapus Produk ini dari Daftar Pembelian?",
                  style: 
                  TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 18.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        // delete
                        minusQtyinCart(idProduk, harga, paramUserID);
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Ya",
                        style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold),
                      )),
                    SizedBox(width: 10.0),
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Tidak",
                        style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold),
                      )),
                  ],
                )
              ],
            )),
        );
      }
    );
  }

  check(String userid, String gt) {
    final form = _key.currentState;
    if(form.validate()) {
      form.save();
      _checkout(userid, gt);
    }
  }

  _checkout(String iduser, String total) async {
    double dTotal = double.parse(total.replaceAll(",", ""));
    double dNilaiBayar = double.parse(nilaiBayar.replaceAll(",", ""));
    double dNilaiKembali = dNilaiBayar - dTotal;

    if(dNilaiBayar >= dTotal) {
      final response = await http.post(Uri.parse(BaseUrl.urlCheckout), body: {
        "userid": iduser,
        "grandtotal": total.replaceAll(",", ""),
        "nilaibayar": nilaiBayar.replaceAll(",", ""),
        "nilaikembali": dNilaiKembali.toString()
      });

      final data = jsonDecode(response.body);
      int value = data['success'];
      String pesans = data['message'];
      if(value == 1) {
        setState(() {
          Navigator.pop(context);
          // link ke halaman berhasil checkout
          Navigator.push(
            context, 
            MaterialPageRoute(builder: (context) => new Checkout()));
        });
      } else {
        Navigator.pop(context);
        print(pesans);
      }
    } else {
      dialogCart("Pembayaran Kurang");
    }
  }

  _addQtyinCart(String idProduk, String harga, String paramUserID) async {
    final response = await http.post(BaseUrl.urlAddCart, 
        body: {"userid": paramUserID, "id_barang": idProduk, "harga": harga});
    final data = jsonDecode(response.body);
    int value = data['success'];
    String pesan = data['message'];

    if(value == 1) {
      print(pesan);
      setState(() {
        getPref();
      });
    } else {
      print(pesan);
      throw StateError('Failed to update Data.');
    }
  }

  minusQtyinCart(String idPoduk, String harga, String paramUserID) async {
    final response = await http.post(Uri.parse(BaseUrl.urlMinusQty),
        body: {"userid": paramUserID, "id_barang": idPoduk, "harga": harga});
    final data = jsonDecode(response.body);
    int value = data['success'];
    String pesan = data['message'];

    if(value == 1) {
      print(pesan);
      setState(() {
        getPref();
      });
    } else {
      print(pesan);
      
    }
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        backgroundColor: Colors.orange,
        title: Text('Detail Belanja'),
        actions: <Widget>[
          new IconButton(
            icon: Icon(Icons.search),
            color: Colors.white,
            onPressed: () {},
            ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _lihatData,
        key: _refresh,
        child: loading
        ? Center(child: CircularProgressIndicator())
        : ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, i) {
            final x = list[i];
            int _currentAmount = int.parse(x.qty);
            int _idbrg = x.idBarang == null ? 0 : int.parse(x.idBarang);
            return Container(
              margin: const EdgeInsets.only(bottom: 5),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.white,
                      ),
                      child: Image.network(BaseUrl.paths + "" + x.gambar,
                          width: 100.0, height: 160.0, fit: BoxFit.fill),
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${x.namaBarang}",
                          style: Theme.of(context).textTheme.title,
                        ),
                        Text(
                          "Rp. " + "${money.format(int.parse(x.harga))}",
                        ),
                        SizedBox(height: 15),
                        Row(
                          children: <Widget>[
                            GestureDetector(
                              child: Container(
                                padding: const EdgeInsets.all(5.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.orange,
                                ),
                                child: Icon(
                                  Icons.remove,
                                  color: Colors.white,
                                ),
                              ),
                              onTap: () {
                                print(BaseUrl.urlMinusQty);
                                if(_currentAmount > 0) {
                                  minusQtyinCart(
                                    x.idBarang, x.harga, x.userId);
                                } else {
                                  _currentAmount = 0;
                                  dialogDelProductInCart(
                                    x.idBarang, x.harga, x.userId);
                                }
                              },
                            ),
                            SizedBox(width: 15),
                            Text(
                              "$_currentAmount",
                              style: Theme.of(context).textTheme.title,
                            ),
                            SizedBox(width: 15),
                            GestureDetector(
                              child: Container(
                                padding: const EdgeInsets.all(5.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.orange
                                ),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                ),
                              ),
                              onTap: () {
                                _addQtyinCart(x.idBarang, x.harga, x.userId);
                              },
                            )
                          ],
                        )
                      ],
                    ))
                ],
              ),
            );
          }
        )
      ),
      bottomNavigationBar: new Container(
        color: Colors.white,
        child: new Row(
          children: <Widget>[
            Expanded(
              child: ListTile(
                title: new Text("Total : "),
                subtitle: 
                new Text("Rp. " + money.format(int.parse(totalBelanja))),
              ),
            ),
            Expanded(
              child: new MaterialButton(
                onPressed: () {
                  totalBelanja != "0"
                  ? dialogCheckout(idUsers, totalBelanja)
                  : dialogCart("Tidak ada Transaksi.");
                },
                child: new Text("Check Out",
                    style: TextStyle(color: Colors.white)),
                color: Colors.orange,
                ),
              ),
          ],
        ),
      ),
    );
  }
}
