import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_ecommerce/model/HistoryModel.dart';

class HistoryDetail extends StatefulWidget {
  final HistoryModel model;
  HistoryDetail(this.model);
  @override
  _HistoryDetailState createState() => _HistoryDetailState();
}

class _HistoryDetailState extends State<HistoryDetail> {
  final price = NumberFormat("#,##0", 'en_US');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Text(
                "Transaksi Detail",
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
            )
          ],
        ),
      ),
      body: ListView(
        padding: EdgeInsets.all(16),
        children: <Widget>[
          Text("Master ID Faktur"),
          Text("ID Faktur : ${widget.model.idfaktur}"),
          Text("Tanggal Jual : ${widget.model.tgljual}"),
          Text("Grand Total : ${widget.model.grandtotal}"),
          Text("Nilai Bayar : ${widget.model.nilaibayar}"),
          Text("Nilai Kembali : ${widget.model.nilaikembali}"),
          Container(
            padding: EdgeInsets.symmetric(vertical: 4),
            child: Divider(
              color: Colors.grey,
            ),
          ),
          ListView.builder(
            itemCount: widget.model.detail.length,
            shrinkWrap: true,
            itemBuilder: (context, i) {
              final a = widget.model.detail[i];
              //var hargaSatuan = int.parse(a.harga) / int.parse(a.qty);
              var totalPrice =
                  int.parse(a.qty) * int.parse(a.harga) / int.parse(a.qty);
              return Row(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Text("ID : ${a.id}"),
                        Text("ID Faktur : ${a.idfaktur}"),
                        Text("ID Barang : ${a.idbarang}"),
                        Text("Nama Barang : ${a.namabarang}"),
                        Text("Quantity : ${a.qty}"),
                        Text(
                            "Harga Satuan : ${price.format(int.parse(a.harga) / int.parse(a.qty))}"),
                        //"Harga Satuan : ${price.format(a.totalPrice)}"),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 4),
                          child: Divider(
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Grand Total: ${price.format(totalPrice)}")
                ],
              );
            },
          )
        ],
      ),
    );
  }
}
