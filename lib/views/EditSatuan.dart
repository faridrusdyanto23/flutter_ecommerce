import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/model/SatuanModel.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_ecommerce/model/api.dart';

class EditSatuan extends StatefulWidget {
  final VoidCallback reload;
  final SatuanModel model;
  EditSatuan(this.model, this.reload);

  @override
  _EditSatuanState createState() => _EditSatuanState();
}

class _EditSatuanState extends State<EditSatuan> {
  String idSatuan, namaSatuan, satuan;

  final _key = new GlobalKey<FormState>();

  TextEditingController txtnamaSatuan, txtSatuan;
  setup() async {
    txtnamaSatuan = TextEditingController(text: widget.model.namaSatuan);
    txtSatuan = TextEditingController(text: widget.model.satuan);
  }

  //tambahan ddl
  // SatuanModel _currentSatuan;
  // final listSatuan = new List<SatuanModel>();
  // final String linkSatuan = BaseUrl.urlListSatuan;

  // Future<List<SatuanModel>> _fetchSatuan() async {
  //   var response = await http.get(Uri.parse(linkSatuan));

  //   if (response.statusCode == 200) {
  //     final items = json.decode(response.body).cast<Map<String, dynamic>>();
  //     List<SatuanModel> listOfSatuan = items.map<SatuanModel>((json) {
  //       return SatuanModel.fromJson(json);
  //     }).toList();

  //     return listOfSatuan;
  //   } else {
  //     throw Exception('Failed to load internet');
  //   }
  // }

  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      prosesbiasa();
    }
  }

  prosesbiasa() async {
    final response = await http.post(BaseUrl.urlEditSatuan, body: {
      "nama_satuan": namaSatuan,
      "satuan": satuan,
      "id_satuan": widget.model.idSatuan
    });
    final data = jsonDecode(response.body);
    int value = data['success'];
    String pesan = data['message'];

    if (value == 1) {
      setState(() {
        print(pesan);
        widget.reload();
        Navigator.pop(context);
      });
    } else {
      print(pesan);
    }
  }

  String body(http.Response response) => response.body;

  // prosesSatuan() async {
  //   try {
  //     var uri = Uri.parse(BaseUrl.urlEditSatuan);
  //     var request = http.MultipartRequest("POST", uri);
  //     request.fields['namaSatuan'] = namaSatuan;
  //     request.fields['idKtg'] = widget.model.idSatuan;
  //   } catch (e) {
  //     prosesbiasa();
  //     debugPrint(e);
  //   }
  // }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setup();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Form(
        key: _key,
        child: ListView(
          padding: EdgeInsets.all(16.0),
          children: <Widget>[
            TextFormField(
              controller: txtnamaSatuan,
              validator: (e) {
                if (e.isEmpty) {
                  return "Silahkan isi data";
                }
              },
              onSaved: (e) => namaSatuan = e,
              decoration: InputDecoration(labelText: "Nama Satuan"),
              style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.normal),
            ),
            TextFormField(
              controller: txtSatuan,
              validator: (e) {
                if (e.isEmpty) {
                  return "Silahkan isi data";
                }
              },
              onSaved: (e) => satuan = e,
              decoration: InputDecoration(labelText: "Jumlah Satuan"),
              style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.normal),
            ),
            MaterialButton(
              onPressed: () {
                check();
              },
              child: Text("Simpan"),
            )
          ],
        ),
      ),
    );
  }
}
