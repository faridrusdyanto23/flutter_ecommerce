class KategoriModel {
  String baris;
  String idKategori;
  String namaKategori;

  KategoriModel(this.baris, this.idKategori, this.namaKategori);

  KategoriModel.fromJson(Map<String, dynamic> json) {
    baris = json['baris'];
    idKategori = json['id_kategori'];
    namaKategori = json['nama_kategori'];
  }
}