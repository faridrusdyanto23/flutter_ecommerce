class ProdukModel {
  String baris;
  String id_barang;
  String id_kategori;
  String nama_kategori;
  String id_satuan;
  String nama_satuan;
  String nama_barang;
  String harga;
  String image;
  String tglexpired;

  ProdukModel(this.baris, this.id_barang, this.id_kategori, this.nama_kategori, this.id_satuan, this.nama_satuan, this.nama_barang, this.harga,
    this.image, this.tglexpired);

  ProdukModel.fromJson(Map<String, dynamic> json) {
    baris = json['baris'];
    id_barang = json['id_barang'];
    id_kategori = json['id_kategori'];
    nama_kategori = json['nama_kategori'];
    id_satuan = json['id_satuan'];
    nama_satuan = json['nama_satuan'];
    nama_barang = json['nama_barang'];
    harga = json['harga'];
    image = json['image'];
    tglexpired = json['tglexpired'];
  }
}