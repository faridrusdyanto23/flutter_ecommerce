class SatuanModel {
  String baris;
  String idSatuan;
  String namaSatuan;
  String satuan;

  SatuanModel(this.baris, this.idSatuan, this.namaSatuan, this.satuan);

  SatuanModel.fromJson(Map<String, dynamic> json) {
    baris = json['baris'];
    idSatuan = json['id_satuan'];
    namaSatuan = json['nama_satuan'];
    satuan = json['satuan'];
  }
}