class CartModel {
  String idBarang;
  String userId;
  String namaBarang;
  String gambar;
  String harga;
  String qty;

  CartModel(this.idBarang, this.userId, this.namaBarang, this.gambar, this.harga, this.qty);

  CartModel.fromJson(Map<String, dynamic> json) {
    idBarang = json['id_barang'];
    userId = json['userid'];
    namaBarang = json['nama_barang'];
    gambar = json['gambar'];
    harga = json['harga'];
    qty = json['qty'];
  }
}