class BaseUrl {
  static String url = "http://192.168.3.184/api_globalshop/";
  static String paths = "http://192.168.3.184/api_globalshop/upload/";

  // data
  static String urlDataBarang = url + "api/data_barang.php";
  // add barang
  static String urlTambahProduk = url + "api/add_barang.php";
  // Edit Data barang
  static String urlEditProduk = url + "api/edit_barang.php";
  // HAPUS DATA kategori
  static String urlHapusProduk = url + "api/delete_barang.php";

  // list kategori
  static String urlListKategori = url + "api/list_kategori.php";
  // add kategori
  static String urlTambahKategori = url + "api/add_kategori.php";
  // HAPUS DATA kategori
  static String urlHapusKategori = url + "api/delete_kategori.php";
  // Edit Data kategori
  static String urlEditKategori = url + "api/edit_kategori.php";

  // list satuan
  static String urlListSatuan = url + "api/list_satuan.php";
  // HAPUS DATA satuan
  static String urlHapusSatuan = url + "api/delete_satuan.php";
  // add satuan
  static String urlTambahSatuan = url + "api/add_satuan.php";
  // Edit Data satuan
  static String urlEditSatuan = url + "api/edit_satuan.php";
  // Add to
  static String urlAddCart = url + "api/add_cart.php";
  static String urlCountCart = url + "api/count_cart.php?userid=";
  static String urlMinusQty = url + "api/minus_qty_cart.php";
  static String urlDetailCart = url + "api/detail_cart.php?userid=";
  // Login
  static String urlLogin = url + "api/login.php";
  // CheckOut
  static String urlCheckout = url + "api/proses_checkout.php";
  // Barang terbaru
  static String urlDataBarangTerbaru = url + "api/data_barang_terbaru.php";
  // Barang terlaris
  static String urlDataBarangTerlaris = url + "api/data_barang_terlaris.php";
  // History Penjualan
  static String urlHistory = url + "api/riwayat.php?userid=";
}
