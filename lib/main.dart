import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/views/LoginPage.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: Colors.indigoAccent[700]),
      home: LoginPage(),
    )
  );
}
