
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/model/ProdukTerlarisModel.dart';
import 'package:flutter_ecommerce/views/DetailProduk.dart';
import 'package:flutter_ecommerce/views/DetailProdukLaris.dart';
import 'package:flutter_ecommerce/views/ListMenu.dart';
import 'package:flutter_ecommerce/views/history.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'model/ProdukModel.dart';
import 'package:flutter_ecommerce/custom/constans.dart';
import 'model/api.dart';
import 'package:flutter_ecommerce/views/Cart.dart';
// add
import 'model/KeranjangModel.dart';

class MenuUser extends StatefulWidget {
  final VoidCallback signOut;
  MenuUser(this.signOut);

  @override
  _MenuUserState createState() => _MenuUserState();
}

class _MenuUserState extends State<MenuUser> {
  signOut() {
    setState(() {
      widget.signOut();
    });
  }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<RefreshIndicatorState> _refresh =
      GlobalKey<RefreshIndicatorState>();

  final money = NumberFormat("#,##0", "en_US");
  var loading = false;
  var loadingLaris = false;
  final listproduk = [];
  final listlaris = [];
  String idUsers;
  final listProduk = new List<ProdukModel>();
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idUsers = preferences.getString("userid");
    });
    _lihatData();
    _countCart();
  }
  Future<void> _lihatData() async {
    listProduk.clear();
    setState(() {
      loading = true;
    });
    final response = await http.get(Uri.parse(BaseUrl.urlDataBarang));
    if (response.contentLength == 2) {
    } else {
      final data = jsonDecode(response.body);
      data.forEach((api) {
        final ab = new ProdukModel(
            api['baris'],
            api['id_barang'],
            api['id_kategori'],
            api['nama_kategori'],
            api['id_satuan'],
            api['nama_satuan'],
            api['nama_barang'],
            api['harga'],
            api['image'],
            api['tglexpired']);
        listProduk.add(ab);
      });

      setState(() {
        loading = false;
      });
    }
  }

  Future<void> _lihatDataLaris() async {
    listlaris.clear();
    setState(() {
      loading = true;
    });
    final response = await http.get(Uri.parse(BaseUrl.urlDataBarangTerlaris));
    if (response.contentLength == 2) {
    } else {
      final dataLaris = jsonDecode(response.body);
      dataLaris.forEach((api) {
        final cd = new ProdukTerlarisModel(
            api['id_barang'],
            api['nama_barang'],
            api['harga'],
            api['image']);
        listlaris.add(cd);
      });

      setState(() {
        loadingLaris = false;
      });
    }
  }

  // add to cart
  tambahKeranjang(String idProduk, String harga) async {
    final response = await http.post(BaseUrl.urlAddCart,
        body: {"userid": idUsers, "id_barang": idProduk, "harga": harga});
    final data = jsonDecode(response.body);
    int value = data['success'];
    String pesan = data['message'];
    if (value == 1) {
      print(pesan);
      _countCart();
    } else {
      print(pesan);
    }
  }

  String jumlahnya = "0";
  final ex = List<KeranjangModel>();
  _countCart() async {
    setState(() {
      loading = true;
    });
    ex.clear();
    final response = await http.get(BaseUrl.urlCountCart + idUsers);
    final data = jsonDecode(response.body);
    data.forEach((api) {
      final exp = new KeranjangModel(api['jumlah']);
      ex.add(exp);
      if (this.mounted) {
        setState(() {
          jumlahnya = exp.jumlah;
        });
      }
    });
    if (this.mounted) {
      setState(() {
        _countCart();
        loading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _lihatData();
    _lihatDataLaris();
    _countCart();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Padding(
          padding: EdgeInsets.only(left: 24),
          child: IconButton(
            onPressed: () => _scaffoldKey.currentState.openDrawer(),
            icon: Icon(
              Icons.menu,
              size: 32,
              color: Colors.black,
            ),
          ),
        ),
        title: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(0.0),
            topRight: Radius.circular(0.0),
          ),
          child: Image.asset("assets/img/logo.png"),
        ),
        actions: <Widget>[
          Stack(
            children: <Widget>[
              IconButton(
                onPressed: () {
                  Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new Cart()));
                },
                icon: Icon(
                  Icons.shopping_cart,
                  color: Colors.orangeAccent,
                ),
              ),
              // tambahkan keterangan jumlah produk dalam cart
              jumlahnya == "0"
                  ? Container()
                  : Positioned(
                      right: 0.0,
                      child: Stack(
                        children: <Widget>[
                          Icon(Icons.brightness_1,
                              size: 20.0, color: Colors.white),
                          Positioned(
                            top: 3.0,
                            right: 6.0,
                            child: Text(
                              jumlahnya,
                              style: TextStyle(
                                  color: Colors.black, fontSize: 11.0),
                            ),
                          ),
                        ],
                      ))
            ],
          )
        ],
      ),
      key: _scaffoldKey,
      body: RefreshIndicator(
        onRefresh: () async {
          _lihatData();
          _lihatDataLaris();
        },
        key: _refresh,
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Container(
            padding: EdgeInsets.only(left: 24, top: 16, bottom: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(
                  height: 12,
                ),
                Text(
                  "Produk Terlaris",
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 300,
                  child: ListView(
                    physics: BouncingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    children: buildItemsLaris(),
                  ),
                ),
                Text(
                  "Produk Terbaru",
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 300,
                  child: ListView(
                    physics: BouncingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    children: buildItems(),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: new Text("Farid R"),
              accountEmail: new Text("Farid@globalshop"),
              currentAccountPicture: new CircleAvatar(
                backgroundImage: AssetImage('assets/img/farid.png'),
              ),
            ),
            ListTile(
              title: Text("Home"),
              onTap: () {},
            ),
            ListTile(
              title: Text("Master Data"),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new ListMenu()));
              },
            ),
            ListTile(
              title: Text("Riwayat Transaksi"),
              onTap: () {
                 Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new History()));
              },
            ),
            ListTile(
              title: Text("Logout"),
              onTap: () {
                setState(() {
                  signOut();
                  print(signOut());
                });
              },
            )

          ],
        ),
      ),
    );
  }

  Widget buildItem(ProdukModel listProduk) {
    return GestureDetector(
      onTap: () {
        // halaman detail
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailProduk(listProduk: listProduk)));
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 200,
            height: 200,
            decoration: BoxDecoration(
                gradient: kGradient,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(
                  width: 1,
                  color: Colors.grey[300],
                )),
            margin: EdgeInsets.only(right: 24),
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: 0,
                    horizontal: 0,
                  ),
                  child: Center(
                    child: Hero(
                        tag: listProduk.nama_barang.length > 20
                            ? listProduk.nama_barang.substring(0, 15) + ' ...'
                            : listProduk.nama_barang,
                        child: Image.network(
                          BaseUrl.paths + listProduk.image,
                          width: 190,
                          fit: BoxFit.contain,
                        )),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    // fitur add to cart
                    tambahKeranjang(listProduk.id_barang, listProduk.harga);
                  },
                  child: Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(25),
                          ),
                        ),
                        width: 60,
                        height: 60,
                        child: Center(
                          child: Icon(
                            Icons.add_shopping_cart,
                            size: 32,
                            color: Colors.black,
                          ),
                        ),
                      )),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Text(
            listProduk.nama_barang.length > 20
                ? listProduk.nama_barang.substring(0, 15) + ' ...'
                : listProduk.nama_barang,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            "Rp " + money.format(int.parse(listProduk.harga)),
            style: TextStyle(
                fontSize: 16,
                color: Colors.orange,
                fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  Widget buildItemLaris(ProdukTerlarisModel listlaris) {
    return GestureDetector(
      onTap: () {
        // halaman detail
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailProdukLaris(listlaris: listlaris)));
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 200,
            height: 200,
            decoration: BoxDecoration(
                gradient: kGradient,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(
                  width: 1,
                  color: Colors.grey[300],
                )),
            margin: EdgeInsets.only(right: 24),
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: 0,
                    horizontal: 0,
                  ),
                  child: Center(
                    child: Hero(
                        tag: listlaris.nama_barang.length > 20
                            ? listlaris.nama_barang.substring(0, 15) + ' ...'
                            : listlaris.nama_barang,
                        child: Image.network(
                          BaseUrl.paths + listlaris.image,
                          width: 190,
                          fit: BoxFit.contain,
                        )),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    // fitur add to cart
                    tambahKeranjang(listlaris.id_barang, listlaris.harga);
                  },
                  child: Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(25),
                          ),
                        ),
                        width: 60,
                        height: 60,
                        child: Center(
                          child: Icon(
                            Icons.add_shopping_cart,
                            size: 32,
                            color: Colors.black,
                          ),
                        ),
                      )),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Text(
            listlaris.nama_barang.length > 20
                ? listlaris.nama_barang.substring(0, 15) + ' ...'
                : listlaris.nama_barang,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            "Rp " + money.format(int.parse(listlaris.harga)),
            style: TextStyle(
                fontSize: 16,
                color: Colors.orange,
                fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  List<Widget> buildItems() {
    List<Widget> list = [];
    for (var listProduk in listProduk) {
      list.add(buildItem(listProduk));
    }
    return list;
  }

  List<Widget> buildItemsLaris() {
    List<Widget> list = [];
    for (var listlaris in listlaris) {
      list.add(buildItemLaris(listlaris));
    }
    return list;
  }
}
